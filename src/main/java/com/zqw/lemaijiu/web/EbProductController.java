package com.zqw.lemaijiu.web;

import com.zqw.lemaijiu.model.EbProduct;
import com.zqw.lemaijiu.model.EbProductCategory;
import com.zqw.lemaijiu.service.EbProductCategoryService;
import com.zqw.lemaijiu.service.EbProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller//迎宾证
@RequestMapping("/product")
public class EbProductController {
    @Autowired
    private EbProductService ebProductService;
    @Autowired
    private EbProductCategoryService ebProductCategoryService;
    @RequestMapping("/detail")
    public String detail(Model model, Integer epId){
        System.out.println("====浏览器点击某个产品===:epId=="+epId);
        //1.调用service
        EbProduct productDetail = ebProductService.detail(epId);
        //1.1
        Map<String, List<EbProductCategory>> categoryMap = ebProductCategoryService.selectBigAndSmallCate();
        //2.把数据绑定到model
        model.addAttribute("detail",productDetail);
        //2.1将商品类分类进行绑定
        model.addAttribute("categoryMap",categoryMap);
        //3.视图名称
    return "product-view";
    }
    //product/cate_product_list/123
    @RequestMapping("/cate_product_list/{cateId}")
   public String productList( Model model,@PathVariable("cateId")Integer cateId,
                             @RequestParam(value = "pSize",defaultValue = "1")Integer pSize,
                             @RequestParam(value = "pageSize",defaultValue = "12")Integer pageSize)  {
        System.out.println("==== 进入productList这个方法 ====cateId:"+cateId);
        //1.查询所有属于二级分类商品
       List<EbProduct> products = ebProductService.selectByCateId(cateId,pSize,pageSize);
        //1.1
        Map<String, List<EbProductCategory>> categoryMap = ebProductCategoryService.selectBigAndSmallCate();

        //将数据绑定到Model中
        model.addAttribute("products",products);
        model.addAttribute("categoryMap",categoryMap);
        //返回视图
        return "product-list";
   }


    @RequestMapping("/add_car")
    public String addCar(HttpSession session, int epId){
        System.out.println("addCar===epId=="+epId);
        //1.调用service根据id查询商品的信息
        EbProduct target_product = ebProductService.detail(epId);
        //2.获取购物车 session
        //3.获取集合 商品--集合--购物车
        List<EbProduct> car = (List<EbProduct>) session.getAttribute("car");
        if(car==null){
            car=new ArrayList<>(10);
        }
        //4.将商品放入集合中
        //4.1判断集合中是否已经存在该商品
        boolean flag = false;//假设购物车车不存在该商品
        for (EbProduct originProduct:car){
            //下面条件成立 表明购物车中存在该商品，只需要修改商品的数量就可以
            //不需要再将该商品添加到购物车
            if(target_product.getEpId().equals(originProduct.getEpId())){
                flag=true;
                originProduct.setCount(originProduct.getCount()+1);
                break;
            }
        }if(!flag){
            car.add(target_product);
        }
        //5.将集合刚入购物车  key-value
        session.setAttribute("car",car);
        return "redirect:/product/datail?epId="+epId;
    }
    @RequestMapping("/show_car")
    public String showCar(){
        return "cart";//shopping.jsp
    }

}
