package com.zqw.lemaijiu.web;

import com.zqw.lemaijiu.model.EbProduct;
import com.zqw.lemaijiu.model.EbProductCategory;
import com.zqw.lemaijiu.service.EbNewsService;
import com.zqw.lemaijiu.service.EbProductCategoryService;
import com.zqw.lemaijiu.service.EbProductService;
import com.zqw.lemaijiu.model.EbNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;




@Controller//迎宾证
@RequestMapping("/front")//工号
public class IndexController {

    @Autowired
    private EbProductService ebProductService;

    @Autowired
    private EbNewsService ebNewsService;

    @Autowired
    private EbProductCategoryService ebProductCategoryService;


    //行为 返回首页的数据(model)和视图(view-html)
    @RequestMapping("/index")//返回前端首页的数据和视图
    public String index(Model model,
                        @RequestParam(value = "pSize", defaultValue = "1") Integer pSize,
                        @RequestParam(value = "pageSize", defaultValue = "15") Integer pageSize) {
        //1.调用service层查询分页的商品信息
        List<EbProduct> ebProductList = ebProductService.selectList(pSize, pageSize);
        //2.调用service层查询分页的新闻信息
        List<EbNews> ebNewsList = ebNewsService.selectList(1, 5);


        //3.调用service层查询商品分类信息
        Map<String, List<EbProductCategory>> categoryMap = ebProductCategoryService.selectBigAndSmallCate();
        //4.将数据绑定model中  key-value进行数据绑定
        model.addAttribute("ebProductList", ebProductList);
        model.addAttribute("ebNewsList", ebNewsList);
        model.addAttribute("categoryMap", categoryMap);


        //5.返回视图(html)
        return "index";//templates/index.html

    }
}
