package com.zqw.lemaijiu.web;


import com.zqw.lemaijiu.model.EbNews;
import com.zqw.lemaijiu.model.EbProductCategory;
import com.zqw.lemaijiu.service.EbNewsService;
import com.zqw.lemaijiu.service.EbProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/news")
public class EbNewsController {
    @Autowired
    private EbNewsService ebNewsService;
    @Autowired
    private EbProductCategoryService ebProductCategoryService;
    @RequestMapping("/detail")
    public String newsdetail(Model model, Integer enId){
        System.out.println("===浏览器点击某个商品====:enId=="+enId);
        //1.调用service查询该商品的详细信息
        EbNews newsdetail = ebNewsService.detail(enId);
        Map<String, List<EbProductCategory>> categoryMap = ebProductCategoryService.selectBigAndSmallCate();
        //把数据绑定到model
        model.addAttribute("detail",newsdetail);
        model.addAttribute("categoryMap",categoryMap);
        //视图名称
        return "index_news";
    }

}
