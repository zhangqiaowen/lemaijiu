package com.zqw.lemaijiu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zqw.lemaijiu.mapper")
public class LemaijiuApplication {

    public static void main(String[] args) {
        SpringApplication.run(LemaijiuApplication.class, args);
    }

}
