package com.zqw.lemaijiu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zqw.lemaijiu.model.EbProductCategory;

import java.util.List;

public interface EbProductCategoryMapper extends BaseMapper<EbProductCategory> {
    //1.查大类
    List<EbProductCategory> selectBigCategory();
    //2.查小类
    List<EbProductCategory> selectSmallCategory();
}
