package com.zqw.lemaijiu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zqw.lemaijiu.model.EbOrder;

public interface EbOrderMapper extends BaseMapper<EbOrder> {
}
