package com.zqw.lemaijiu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zqw.lemaijiu.model.EbOrderDetail;

public interface EbOrderDetailMapper extends BaseMapper<EbOrderDetail> {
}
