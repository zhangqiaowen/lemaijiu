package com.zqw.lemaijiu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zqw.lemaijiu.model.EbComment;

public interface EbCommentMapper extends BaseMapper<EbComment> {
}
