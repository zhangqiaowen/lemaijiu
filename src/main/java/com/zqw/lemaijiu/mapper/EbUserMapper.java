package com.zqw.lemaijiu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zqw.lemaijiu.model.EbUser;

public interface EbUserMapper extends BaseMapper<EbUser> {
}
