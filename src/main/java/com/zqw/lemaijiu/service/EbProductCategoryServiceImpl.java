package com.zqw.lemaijiu.service;


import com.zqw.lemaijiu.mapper.EbProductCategoryMapper;
import com.zqw.lemaijiu.model.EbProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service//业务员证
public class EbProductCategoryServiceImpl implements EbProductCategoryService {

    @Autowired
    private EbProductCategoryMapper ebProductCategoryMapper;
    @Override
    public Map<String, List<EbProductCategory>> selectBigAndSmallCate() {
        //浏览器--web--service-mapper-数据库
        //1.查询出所有大类
        List<EbProductCategory> bigCategory = ebProductCategoryMapper.selectBigCategory();


        //2.查询出所有的小类
        List<EbProductCategory> smallCategory = ebProductCategoryMapper.selectSmallCategory();

        //3.将大类和小类放到map集合
        Map<String, List<EbProductCategory>> categoryMap = new HashMap();
        categoryMap.put("big",bigCategory);
        categoryMap.put("small",smallCategory);

        return categoryMap;
    }
}