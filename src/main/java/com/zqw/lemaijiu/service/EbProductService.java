package com.zqw.lemaijiu.service;

import com.zqw.lemaijiu.model.EbProduct;

import java.util.List;

public interface EbProductService {
    //分页查询商品信息
    List<EbProduct>selectList(Integer page, Integer pageSize);
    /*
    根据Id查询商品的详细信息
     */
    EbProduct detail(Integer epId);

    List<EbProduct> selectByCateId(Integer cateId, Integer pSize, Integer pageSize);
}
