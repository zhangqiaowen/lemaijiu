package com.zqw.lemaijiu.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zqw.lemaijiu.mapper.EbNewsMapper;
import com.zqw.lemaijiu.model.EbNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



@Service//业务员的证
public class EbNewsServiceImpl implements EbNewsService {

    @Autowired
    private EbNewsMapper ebNewsMapper;

    @Override
    public List<EbNews> selectList(Integer pSize, Integer pageSize) {
        //浏览器--->web---service---mapper---数据库
        Page<EbNews> page = new Page(pSize,pageSize);
        Page<EbNews> ebNewsPage = ebNewsMapper.selectPage(page,
                Wrappers.<EbNews>lambdaQuery().
                        orderByDesc(EbNews::getEnCreateTime));//时间降序
        return ebNewsPage.getRecords();
    }

    @Override
    public EbNews detail(Integer enId) {
        return  ebNewsMapper.selectById(enId);

    }
}
