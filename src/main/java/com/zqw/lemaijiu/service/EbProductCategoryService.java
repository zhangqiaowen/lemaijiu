package com.zqw.lemaijiu.service;

import com.zqw.lemaijiu.model.EbProductCategory;

import java.util.List;
import java.util.Map;


/**

 * Map map = new HashMap();
 *
 * map.put("big",大类集合)
 * map.put("small",小类集合）
 *
 */
public interface EbProductCategoryService {
    Map<String, List<EbProductCategory>> selectBigAndSmallCate();
}
