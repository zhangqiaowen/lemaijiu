package com.zqw.lemaijiu.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zqw.lemaijiu.mapper.EbProductMapper;
import com.zqw.lemaijiu.model.EbProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service //业务员证
public class EbProductServiceImpl implements EbProductService {

    @Autowired
    private EbProductMapper ebProductMapper;

    /**
     *
     * @param psize     第几页
     * @param pageSize  每页显示多少条
     * @return
     */
    @Override
    public List<EbProduct> selectList(Integer psize, Integer pageSize) {
        //web---service---maper---数据库
        //第2页 ，每页5条
        Page<EbProduct> page = new Page<>(psize, pageSize);
        //null  ： 根据什么条件分页， null不根据任何条件分页
        Page<EbProduct> productPage = ebProductMapper.selectPage(page, null);

        return productPage.getRecords();
    }

    @Override
    public EbProduct detail(Integer epId) {
        //web-service-mapper-数据库
        return  ebProductMapper.selectById(epId);
    }

    @Override
    public List<EbProduct> selectByCateId(Integer cateId, Integer pSize, Integer pageSize) {
       Page< EbProduct> page= new Page();
        Page<EbProduct> ebProductList = ebProductMapper.selectPage(page, Wrappers.<EbProduct>lambdaQuery()
                .eq(EbProduct::getEpcChildId, cateId));
        return ebProductList.getRecords();
    }

}

