package com.zqw.lemaijiu.service;
import com.zqw.lemaijiu.model.EbNews;

import java.util.List;

public interface EbNewsService {
    List<EbNews> selectList(Integer pSize, Integer pageSize);
    EbNews detail(Integer enId);
}